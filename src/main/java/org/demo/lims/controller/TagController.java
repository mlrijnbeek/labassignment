package org.demo.lims.controller;

import org.demo.lims.model.Tag;
import org.demo.lims.service.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/lims-demo")
public class TagController {

    @Autowired
    private TagService tagService;

    private static final Logger logger = LoggerFactory.getLogger(TagController.class);

    @PostMapping(value = "/tag")
    public Tag createTag(@Valid @RequestBody Tag tag) {
        tagService.create(tag);
        return tag;
    }

    @PutMapping(value = "/tag/{tagId}/sample/{sampleId}")
    public String addTagToSample(@PathVariable Long tagId, @PathVariable Long sampleId) {
        tagService.tagSample(tagId,sampleId);
        return "success";
    }

}
