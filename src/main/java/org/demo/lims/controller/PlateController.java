package org.demo.lims.controller;

import org.demo.lims.model.Plate;
import org.demo.lims.service.PlateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/lims-demo")
public class PlateController {

    @Autowired
    private PlateService plateService;

    private static final Logger logger = LoggerFactory.getLogger(PlateController.class);

    @PostMapping(value = "/plate")
    public Plate createPlate(
            @Valid @RequestBody Plate plate) {
        plateService.create(plate);
        return plate;
    }

    @GetMapping(value = "/plate/{id}")
    public Plate findSampleTubeById(@PathVariable Long id) {
        return plateService.findPlateById(id);
    }

    @GetMapping(value = "/plate/barcode/{barCode}")
    public List<Plate> findSampleTubeByBarcode(@PathVariable String barCode) {
        return plateService.findPlateByBarCode(barCode);
    }


}
