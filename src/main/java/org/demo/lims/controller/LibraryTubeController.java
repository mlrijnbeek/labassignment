package org.demo.lims.controller;

import org.demo.lims.model.LibraryTube;
import org.demo.lims.model.Sample;
import org.demo.lims.service.LibraryTubeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/lims-demo")
public class LibraryTubeController {

    @Autowired
    private LibraryTubeService libraryTubeService;

    private static final Logger logger = LoggerFactory.getLogger(LibraryTubeController.class);

    @PostMapping(value = "/library-tube")
    public LibraryTube createLibraryTube(
            @Valid @RequestBody LibraryTube libraryTube) {
        libraryTubeService.create(libraryTube);
        return libraryTube;
    }

    @GetMapping(value = "/library-tube/{id}")
    public LibraryTube findSampleTubeById(@PathVariable Long id) {
        return libraryTubeService.findLibraryTubeById(id);
    }

    @GetMapping(value = "/library-tube/barcode/{barCode}")
    public List<LibraryTube> findSampleTubeByBarcode(@PathVariable String barCode) {
        return libraryTubeService.findLibraryTubeByBarCode(barCode);
    }

    @PostMapping(value = "/library-tube/{libraryTubeId}/sample")
    public String addSampleToLibraryTube(
            @Valid @RequestBody Sample sample, @PathVariable Long libraryTubeId) {
        libraryTubeService.addSample(libraryTubeId, sample);
        return "success"; // TODO: fix H2 cache and return LibraryTube
    }

    @DeleteMapping(value = "/library-tube/{libraryTubeId}/sample/{sampleId}")
    public String deleteSampleFromLibraryTube(@PathVariable Long libraryTubeId, @PathVariable Long sampleId) {
        libraryTubeService.removeSample(libraryTubeId, sampleId);
        return "success"; // TODO: fix H2 cache and return LibraryTube
    }


    @PutMapping(value = "/library-tube/move-all-samples/from/{fromTubeId}/to/{toTubeId}")
    public String moveAllSamples(@PathVariable Long fromTubeId, @PathVariable Long toTubeId) {
        libraryTubeService.moveAllSamples(fromTubeId,toTubeId);
        return "success";
    }

    @PutMapping(value = "/library-tube/move-sample/from/{fromTubeId}/to/{toTubeId}/sample/{sampleId}")
    public String moveSample(@PathVariable Long fromTubeId, @PathVariable Long toTubeId, @PathVariable Long sampleId) {
        libraryTubeService.moveSample(fromTubeId,toTubeId, sampleId);
        return "success";
    }


}
