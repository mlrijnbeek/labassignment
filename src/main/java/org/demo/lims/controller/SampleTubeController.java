package org.demo.lims.controller;

import org.demo.lims.model.SampleTube;
import org.demo.lims.service.SampleTubeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import javax.validation.Valid;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/lims-demo")
public class SampleTubeController {

    @Autowired
    private SampleTubeService sampleTubeService;

    private static final Logger logger = LoggerFactory.getLogger(SampleTubeController.class);

    @PostMapping(value = "/sample-tube")
    public SampleTube createSampleTube(
            @Valid @RequestBody SampleTube sampleTube) {
        sampleTubeService.create(sampleTube);
        return sampleTube;
    }

    @GetMapping(value = "/sample-tube/{id}")
    public SampleTube findSampleTubeById(@PathVariable Long id) {
        return sampleTubeService.findSampleTubeById(id);
    }

    @GetMapping(value = "/sample-tube/barcode/{barCode}")
    public List<SampleTube> findSampleTubeByBarcode(@PathVariable String barCode) {
        return sampleTubeService.findSampleTubeByBarCode(barCode);
    }

}
