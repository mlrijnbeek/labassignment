package org.demo.lims.dao;

import org.demo.lims.model.LibraryTube;

import java.util.List;

public interface LibraryTubeDao {

    void create(LibraryTube libraryTube);

    void update(LibraryTube libraryTube);

    LibraryTube getLibraryTubeById(long id);

    void delete(long id);

    List<LibraryTube> getLibraryTubeByBarCode(String barCode);

    void refresh(LibraryTube libraryTube);

}
