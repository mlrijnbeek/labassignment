package org.demo.lims.dao;

import org.demo.lims.model.Well;

public interface WellDao {

    void create(Well well);

    void update(Well well);

    Well getWellById(long id);

    void delete(long id);
}
