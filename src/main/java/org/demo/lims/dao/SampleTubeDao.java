package org.demo.lims.dao;

import org.demo.lims.model.SampleTube;
import java.util.List;

public interface SampleTubeDao {

    void create(SampleTube sampleTube);

    void update(SampleTube sampleTube);

    SampleTube getSampleTubeById(long id);

    void delete(long id);

    List<SampleTube> getSampleTubeByBarCode(String barCode);

}
