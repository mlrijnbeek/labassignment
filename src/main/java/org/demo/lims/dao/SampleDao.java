package org.demo.lims.dao;

import org.demo.lims.model.Sample;

public interface SampleDao {

    void create(Sample sample);

    void update(Sample sample);

    Sample getSampleById(long id);

    void delete(long id);
}
