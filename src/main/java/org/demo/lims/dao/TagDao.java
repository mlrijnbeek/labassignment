package org.demo.lims.dao;

import org.demo.lims.model.Tag;

public interface TagDao {

    void create(Tag tag);

    void update(Tag tag);

    Tag getTagById(long id);

    void delete(long id);
}
