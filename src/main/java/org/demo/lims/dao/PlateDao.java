package org.demo.lims.dao;

import org.demo.lims.model.Plate;
import java.util.List;

public interface PlateDao {

    void create(Plate Plate);

    void update(Plate Plate);

    Plate getPlateById(long id);

    void delete(long id);

    List<Plate> findPlateByBarCode(String barCode);
}



