package org.demo.lims.dao.impl;

import org.demo.lims.dao.LibraryTubeDao;
import org.demo.lims.model.LibraryTube;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional

public class LibraryTubeDaoImpl implements LibraryTubeDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(LibraryTube libraryTube) {
        entityManager.persist(libraryTube);
    }

    @Override
    public void update(LibraryTube libraryTube) {

        entityManager.merge(libraryTube);
    }

    @Override
    public LibraryTube getLibraryTubeById(long id) {
        LibraryTube lt = entityManager.find(LibraryTube.class, id);
        return lt;
    }

    @Override
    public List<LibraryTube> getLibraryTubeByBarCode(String barCode) {
            return entityManager.createQuery(
                    "SELECT s FROM LibraryTube s WHERE s.barCode LIKE :bc")
                    .setParameter("bc", barCode)
                    .getResultList();
    }


    @Override
    public void delete(long id) {
        LibraryTube libraryTube = getLibraryTubeById(id);
        if (libraryTube != null) {
            entityManager.remove(libraryTube);
        }
    }

    @Override
    public void refresh(LibraryTube libraryTube) {
        entityManager.refresh(libraryTube);
    }

}
