package org.demo.lims.dao.impl;

import org.demo.lims.dao.PlateDao;
import org.demo.lims.model.Plate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional

public class PlateDaoImpl implements PlateDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Plate plate) {

        entityManager.persist(plate);
    }

    @Override
    public void update(Plate plate) {
        entityManager.merge(plate);
    }

    @Override
    public Plate getPlateById(long id) {
        return entityManager.find(Plate.class, id);
    }

    @Override
    public List<Plate> findPlateByBarCode(String barCode) {
        return entityManager.createQuery(
                "SELECT s FROM Plate s WHERE s.barCode LIKE :bc")
                .setParameter("bc", barCode)
                .getResultList();

    }

    @Override
    public void delete(long id) {
        Plate plate = getPlateById(id);
        if (plate != null) {
            entityManager.remove(plate);
        }


    }
}
