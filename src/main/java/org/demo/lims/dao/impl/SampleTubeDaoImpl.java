package org.demo.lims.dao.impl;

import org.demo.lims.dao.SampleTubeDao;
import org.demo.lims.model.SampleTube;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

@Repository
@Transactional

public class SampleTubeDaoImpl implements SampleTubeDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(SampleTube sampleTube) {
        entityManager.persist(sampleTube);
    }

    @Override
    public void update(SampleTube sampleTube) {
        entityManager.merge(sampleTube);
    }

    @Override
    public SampleTube getSampleTubeById(long id) {
        return entityManager.find(SampleTube.class, id);
    }

    @Override
    public List<SampleTube> getSampleTubeByBarCode(String barCode) {
            return entityManager.createQuery(
                    "SELECT s FROM SampleTube s WHERE s.barCode LIKE :bc")
                    .setParameter("bc", barCode)
                    .getResultList();
    }


    @Override
    public void delete(long id) {
        SampleTube sampleTube = getSampleTubeById(id);
        if (sampleTube != null) {
            entityManager.remove(sampleTube);
        }
    }

}
