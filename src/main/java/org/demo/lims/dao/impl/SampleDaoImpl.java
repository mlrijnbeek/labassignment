package org.demo.lims.dao.impl;

import org.demo.lims.dao.SampleDao;
import org.demo.lims.model.Sample;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional

public class SampleDaoImpl implements SampleDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Sample sample) {

        entityManager.persist(sample);
    }

    @Override
    public void update(Sample sample) {
        entityManager.merge(sample);
    }

    @Override
    public Sample getSampleById(long id) {
        return entityManager.find(Sample.class, id);
    }

    @Override
    public void delete(long id) {
        Sample sample = getSampleById(id);
        if (sample != null) {
            entityManager.remove(sample);
        }
    }
}
