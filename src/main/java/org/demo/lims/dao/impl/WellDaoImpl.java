package org.demo.lims.dao.impl;

import org.demo.lims.dao.WellDao;
import org.demo.lims.model.Well;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional

public class WellDaoImpl implements WellDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Well well) {

        entityManager.persist(well);
    }

    @Override
    public void update(Well well) {
        entityManager.merge(well);
    }

    @Override
    public Well getWellById(long id) {
        return entityManager.find(Well.class, id);
    }

    @Override
    public void delete(long id) {
        Well well = getWellById(id);
        if (well != null) {
            entityManager.remove(well);
        }
    }
}
