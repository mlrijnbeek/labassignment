package org.demo.lims.dao.impl;

import org.demo.lims.dao.TagDao;
import org.demo.lims.model.Tag;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional

public class TagDaoImpl implements TagDao{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Tag tag) {

        entityManager.persist(tag);
    }

    @Override
    public void update(Tag tag) {
        entityManager.merge(tag);
    }

    @Override
    public Tag getTagById(long id) {
        return entityManager.find(Tag.class, id);
    }

    @Override
    public void delete(long id) {
        Tag tag = getTagById(id);
        if (tag != null) {
            entityManager.remove(tag);
        }
    }
}
