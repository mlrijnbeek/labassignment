package org.demo.lims.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Set;

/**
 *  Library tube
 *  - Has a Barcode begins with NT,
 *  - Contains one or more uniquely tagged samples.
 */
@Entity
@Table(name = "librarytube")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LibraryTube extends Container {

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "libraryTube")
    private Set<Sample> samples;

    @ManyToOne(fetch = FetchType.LAZY,optional = true)
    @JoinColumn(name = "labware_id")
    Labware labWare;

    public Set<Sample> getSamples() {
        return samples;
    }

    public void setSamples(Set<Sample> samples) {
        this.samples = samples;
    }

    public Labware getLabWare() {
        return labWare;
    }

    public void setLabWare(Labware labWare) {
        this.labWare = labWare;
    }
}
