package org.demo.lims.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

/**
 * Must have a name
 * - Names must be unique
 * - Must be in a container
 * - Can have at most one tag applied to it
 */

@Entity
@Table(name = "sample", uniqueConstraints = {@UniqueConstraint(columnNames = "name", name = "UNIQUE_SAMPLE_NAME")})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Sample {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @JsonIgnore
    @ManyToOne //(fetch = FetchType.LAZY)
    @JoinColumn(name = "tag_id")
    Tag tag;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    public SampleTube  sampleTube;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "well_id")
    Well well;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "library_tube_id")
    LibraryTube libraryTube;

    @Transient
    String tagSequence;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SampleTube getSampleTube() {
        return sampleTube;
    }

    public void setSampleTube(SampleTube sampleTube) {
        this.sampleTube = sampleTube;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Well getWell() {
        return well;
    }

    public void setWell(Well well) {
        this.well = well;
    }

    public LibraryTube getLibraryTube() {
        return libraryTube;
    }

    public void setLibraryTube(LibraryTube libraryTube) {
        this.libraryTube = libraryTube;
    }

    public String getTagSequence() {
        return tag!=null?tag.getSequence():null;
    }

    public void setTagSequence(String tagSequence) { }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sample sample = (Sample) o;

        return name != null ? name.equals(sample.name) : sample.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}

