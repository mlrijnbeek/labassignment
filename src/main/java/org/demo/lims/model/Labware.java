package org.demo.lims.model;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Set;

/**
 * Labware
 * - Can have a barcode,
 * - Can be comprised of one or many containers,
 * - Can have state - pending, started, passed and failed, which can be changed.
 * - Can move from pending to started and then started to passed or failed.
 * - Plate, Sample Tube, Library Tube and Flowcell are all types of Labware
 */
@Entity
@Table(name = "labware")
public class Labware {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String barCode;

    @Enumerated(EnumType.STRING)
    @Column
    private State state;

    // Plate, Sample Tube, Library Tube and Flowcell are all types of Labware
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "labWare" )
    private Set<Plate> plates;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "labWare" )
    private Set<SampleTube> sampleTubes;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "labWare" )
    private Set<LibraryTube> libraryTubes;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "labWare" )
    private Set<FlowCell> flowCells;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Set<Plate> getPlates() {
        return plates;
    }

    public void setPlates(Set<Plate> plates) {
        this.plates = plates;
    }

    public Set<SampleTube> getSampleTubes() {
        return sampleTubes;
    }

    public void setSampleTubes(Set<SampleTube> sampleTubes) {
        this.sampleTubes = sampleTubes;
    }

    public Set<LibraryTube> getLibraryTubes() {
        return libraryTubes;
    }

    public void setLibraryTubes(Set<LibraryTube> libraryTubes) {
        this.libraryTubes = libraryTubes;
    }

    public Set<FlowCell> getFlowCells() {
        return flowCells;
    }

    public void setFlowCells(Set<FlowCell> flowCells) {
        this.flowCells = flowCells;
    }
}
