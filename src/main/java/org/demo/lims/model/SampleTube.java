package org.demo.lims.model;

import javax.persistence.*;

/**
 *  Sample Tube
 *   - Has a Barcode beginning with NT,
 *   - Contains one sample only.
 */
@Entity
@Table(name = "sampletube")
public class SampleTube extends Container {

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "sampleTube", cascade = CascadeType.ALL)
    private Sample sample;

    @ManyToOne(fetch = FetchType.LAZY,optional = true)
    @JoinColumn(name = "labware_id")
    Labware labWare;

    public Sample getSample() {
        return sample;
    }

    public void setSample(Sample sample) {
        this.sample = sample;
    }

    public Labware getLabWare() {
        return labWare;
    }

    public void setLabWare(Labware labWare) {
        this.labWare = labWare;
    }
}
