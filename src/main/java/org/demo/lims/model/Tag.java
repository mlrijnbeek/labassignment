package org.demo.lims.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**

 Must have a DNA sequence which can uniquely identify it (e.g. ATTGGCAT),

 Once two or more samples have had tags applied and have been put in the
 same library tube or well, they cannot be moved into another tube or well
 without all of the other samples that are in the same tube or well being
 moved with them

 A tag can be applied to multiple samples
 */

@Entity
@Table(name = "tag", uniqueConstraints = {@UniqueConstraint(columnNames = "sequence")})

public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    String sequence;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tag")
    private Set<Sample> samples;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public Set<Sample> getSamples() {
        return samples;
    }

    public void setSamples(Set<Sample> samples) {
        this.samples = samples;
    }
}
