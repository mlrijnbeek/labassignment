package org.demo.lims.model;

import javax.persistence.*;
import java.util.Collection;


/**
 *  A two-dimensional array of 96 or 384 wells
 *  - Has a Barcode beginning with DN
 */

@Entity
@Table(name = "plate")
public class Plate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String barCode;

    @OneToMany(mappedBy = "plate", fetch = FetchType.EAGER)
    private Collection<Well> wells;

    @ManyToOne(fetch = FetchType.LAZY,optional = true)
    @JoinColumn(name = "labware_id")
    Labware labWare;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Collection<Well> getWells() {
        return wells;
    }

    public void setWells(Collection<Well> wells) {
        this.wells = wells;
    }

    public Labware getLabWare() {
        return labWare;
    }

    public void setLabWare(Labware labWare) {
        this.labWare = labWare;
    }
}
