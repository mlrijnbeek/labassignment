package org.demo.lims.model;

public enum State {

    PENDING, STARTED, PASSED, FAILED;
}
