package org.demo.lims.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Flow cell:
 *  - No barcode,
 *  - Consists of two or eight lanes
 *
 */
//TODO validate 2-8

@Entity
@Table(name = "flowcell")
public class FlowCell {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "flowCell")
    private Set<Lane> lanes;

    @ManyToOne(fetch = FetchType.LAZY,optional = true)
    @JoinColumn(name = "labware_id")
    Labware labWare;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(Set<Lane> lanes) {
        this.lanes = lanes;
    }

    public Labware getLabWare() {
        return labWare;
    }

    public void setLabWare(Labware labWare) {
        this.labWare = labWare;
    }
}
