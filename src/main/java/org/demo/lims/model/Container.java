package org.demo.lims.model;

import javax.persistence.*;

/**
 Container
 - Contains none to many samples,
 - Can have a location within a piece of labware,
 - Samples can be copied from one container to another,
 - Tags can be applied to all samples within a container,
 - Cannot contain two samples with the same tag,
 - May or may not have a barcode.
 - Well, Sample Tube, Library Tube and Lane are all types of Container.
 */

//TODO copy
//TODO cannot contain two samples with the same tag

@MappedSuperclass
public abstract class Container {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String barCode;

    @Column
    private String labwareLocation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getLabwareLocation() {
        return labwareLocation;
    }

    public void setLabwareLocation(String labwareLocation) {
        this.labwareLocation = labwareLocation;
    }
}
