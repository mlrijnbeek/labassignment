package org.demo.lims.model;

import javax.persistence.*;
import java.util.Set;

/**
 *  Lane
 *  - No barcode,
 *  - Will have a state of pass or fail,
 *  - Must have a numerical identifier (1..N) called position,
 *  - Can contain one or more tagged samples.
 */

// TODO: validation in LaneService checks only pass/fail

@Entity
@Table(name = "lane")
public class Lane extends Container {

    @Enumerated(EnumType.STRING)
    @Column
    private State state;

    @Column
    private Integer position;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tag")
    private Set<Sample> samples;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flowcell_id")
    FlowCell flowCell;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Set<Sample> getSamples() {
        return samples;
    }

    public void setSamples(Set<Sample> samples) {
        this.samples = samples;
    }

    public void setBarCode(String barCode) {
        // Lane bar code is always null
    }

}
