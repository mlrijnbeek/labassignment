package org.demo.lims.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.Set;

/**
 * Must have a co-ordinate within a plate (A1, B6 etc.)
 *  - Is similar to a library tube or sample tube but has no barcode.
 */
@Entity
@Table(name = "well")
public class Well extends Container{

    @Column (nullable = false)
    String wellCoordinate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plate_id", nullable = false)
    @JsonIgnore
    private Plate plate;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "well")
    private Set<Sample> samples;

    public String getWellCoordinate() {
        return wellCoordinate;
    }

    public void setWellCoordinate(String wellCoordinate) {
        this.wellCoordinate = wellCoordinate;
    }

    public void setBarCode(String barCode) {
        setBarCode(null);
    }

    public Plate getPlate() {
        return plate;
    }

    public void setPlate(Plate plate) {
        this.plate = plate;
    }

    public Set<Sample> getSamples() {
        return samples;
    }

    public void setSamples(Set<Sample> samples) {
        this.samples = samples;
    }
}
