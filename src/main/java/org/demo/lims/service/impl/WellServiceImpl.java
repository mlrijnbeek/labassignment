package org.demo.lims.service.impl;

import org.demo.lims.dao.WellDao;
import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Well;
import org.demo.lims.service.ContainerService;
import org.demo.lims.service.WellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class WellServiceImpl implements WellService {

    @Autowired
    private WellDao wellDao;

    @Autowired
    private ContainerService containerService;

    @Override
    public void create(Well well) {
        validate(well);
        wellDao.create(well);
    }

    @Override
    public void validate(Well well) {
        containerService.tagCheck(well.getSamples());
    }

}
