package org.demo.lims.service.impl;

import org.demo.lims.dao.LibraryTubeDao;
import org.demo.lims.dao.SampleTubeDao;
import org.demo.lims.exception.DataNotFoundException;
import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.LibraryTube;
import org.demo.lims.model.Sample;
import org.demo.lims.service.ContainerService;
import org.demo.lims.service.LibraryTubeService;
import org.demo.lims.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

@Service
public class LibraryTubeServiceImpl implements LibraryTubeService {

    @Autowired
    private LibraryTubeDao libraryTubeDao;

    @Autowired
    private SampleService sampleService;

    @Autowired
    private ContainerService containerService;

    @Override
    @Transactional
    public void create(LibraryTube libraryTube) {
        validate(libraryTube);

        libraryTubeDao.create(libraryTube);
        if (libraryTube.getSamples()!=null) {
            for (Sample sample : libraryTube.getSamples()) {
                sample.setLibraryTube(libraryTube);
                sampleService.create(sample);
            }
        }
        libraryTube = libraryTubeDao.getLibraryTubeById(libraryTube.getId());
    }

    @Override
    @Transactional
    public LibraryTube findLibraryTubeById(long id) {
        return libraryTubeDao.getLibraryTubeById(id);
    }

    @Override
    @Transactional
    public List<LibraryTube> findLibraryTubeByBarCode(String barCode) {
        return libraryTubeDao.getLibraryTubeByBarCode(barCode);
    }

    @Override
    @Transactional
    public void addSample(Long libraryTubeId, Sample sample) {
        LibraryTube libraryTube = libraryTubeDao.getLibraryTubeById(libraryTubeId);
        if (libraryTube!=null) {
            sample.setLibraryTube(libraryTube);
            sampleService.create(sample);
        }
        else {
            throw new DataNotFoundException("Library tube with id " + libraryTubeId + " does not exist");
        }
    }

    @Override
    @Transactional
    public void removeSample(Long libraryTubeId, Long sampleId) {
        //To do: check that libraryTubeId and sampleId are existing id's
        sampleService.delete(sampleId);
    }

    private void refresh(LibraryTube libraryTube) {

    }

    @Override
    public void validate(LibraryTube libraryTube) {

        if (libraryTube.getBarCode()==null) {
            throw new ValidationException("Library tube barcode must not be null");
        }
        if (!libraryTube.getBarCode().startsWith("NT")) {
            throw new ValidationException("Library tube barcode must start with NT");
        }

        containerService.tagCheck(libraryTube.getSamples());

    }

    @Override
    public void moveAllSamples(Long fromTubeId, Long toTubeId) {
        LibraryTube fromLibraryTube = libraryTubeDao.getLibraryTubeById(fromTubeId);
        LibraryTube toLibraryTube = libraryTubeDao.getLibraryTubeById(toTubeId);

        if (fromLibraryTube!=null && toLibraryTube!=null) {
            if (fromLibraryTube.getSamples()!=null) {

                if (toLibraryTube==null) {
                    toLibraryTube.setSamples(new HashSet<>());
                }
                for (Sample s : fromLibraryTube.getSamples()) {
                    toLibraryTube.getSamples().add(s);
                    s.setLibraryTube(toLibraryTube); //You need to maintain both sides of a bi-directional relationship.
                }

                fromLibraryTube.setSamples(null);
                libraryTubeDao.update(fromLibraryTube);
                libraryTubeDao.update(toLibraryTube);
            }
        }
        else {
            throw new DataNotFoundException("Library tube IDs could not be resolved to existing tubes.");
        }

    }


    /*
    Once two or more samples have had tags applied and have been put in the
    same library tube or well, they cannot be moved into another tube or well
    without all of the other samples that are in the same tube or well being
    moved with them,
    */
    @Override
    public void moveSample(Long fromTubeId, Long toTubeId, Long sampleId) {
        LibraryTube fromLibraryTube = libraryTubeDao.getLibraryTubeById(fromTubeId);
        LibraryTube toLibraryTube = libraryTubeDao.getLibraryTubeById(toTubeId);
        Sample sample = sampleService.getSampleById(sampleId);

        if (fromLibraryTube==null || toLibraryTube ==null || sample ==null) {
            throw new DataNotFoundException("Library tube IDs and/or sample ID could not be resolved to existing entities");
        }
        else {
            if (fromLibraryTube.getSamples()==null || !fromLibraryTube.getSamples().contains(sample) ) {
                throw new DataNotFoundException("Source library tube does not contain the given sample");
            }
            if (sampleService.isSampleSetWithMutipleTags(fromLibraryTube.getSamples())) {
                throw new ValidationException("Cannot move a sample out of a tube with two or more samples tagged");
            }

            if (toLibraryTube.getSamples()==null) {
                toLibraryTube.setSamples(new HashSet<>());
            }
            toLibraryTube.getSamples().add(sample);
            sample.setLibraryTube(toLibraryTube); //You need to maintain both sides of a bi-directional relationship.
            fromLibraryTube.getSamples().remove(sample);

            libraryTubeDao.update(fromLibraryTube);
            libraryTubeDao.update(toLibraryTube);

        }

    }


}
