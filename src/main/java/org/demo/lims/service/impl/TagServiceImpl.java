package org.demo.lims.service.impl;

import org.demo.lims.dao.SampleDao;
import org.demo.lims.dao.TagDao;
import org.demo.lims.exception.DataNotFoundException;
import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Sample;
import org.demo.lims.model.Tag;
import org.demo.lims.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.regex.*;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDao tagDao;

    @Autowired
    private SampleDao sampleDao;

    @Override
    public void create(Tag tag) {
        validate(tag);
        tagDao.create(tag);
    }

    @Override
    public void tagSample(Long tagId, Long sampleId) {
        Tag tag = tagDao.getTagById(tagId);
        Sample sample = sampleDao.getSampleById(sampleId);
        if (tag==null || sample == null ) {
            throw new DataNotFoundException("Tag and/or sample not found");
        }

        if (tag.getSamples()==null) {
            tag.setSamples(new HashSet<>());
        }
        sample.setTag(tag);
        tag.getSamples().add(sample);
        tagDao.update(tag);

    }

    @Override
    public void validate(Tag tag) {

        Pattern p = Pattern.compile("^[ACGT]+$");
        Matcher m = p.matcher(tag.getSequence());
        if (!m.matches()) {
            throw new ValidationException("Not a valid DNA sequence: " + tag.getSequence());
        }
    }

    @Override
    public Tag getTagById(long id) {
        return tagDao.getTagById(id);
    }

}
