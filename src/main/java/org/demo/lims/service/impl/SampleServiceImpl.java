package org.demo.lims.service.impl;

import org.demo.lims.dao.SampleDao;
import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Sample;
import org.demo.lims.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@Transactional
public class SampleServiceImpl implements SampleService {

    @Autowired
    private SampleDao sampleDao;

    @Override
    public void create(Sample sample) {
        validate(sample);
        sampleDao.create(sample);
    }

    @Override
    public void validate(Sample sample) {
        if (sample.getLibraryTube()==null && sample.getSampleTube()==null && sample.getWell()==null) {
            throw new ValidationException("Sample must belong to a container");
        }
    }

    @Override
    public Sample getSampleById(long id) {
        return sampleDao.getSampleById(id);
    }

    @Override
    public void delete(long id) {
        sampleDao.delete(id);
    }

    @Override
    public boolean isSampleSetWithMutipleTags(Set<Sample> samples) {
        if (samples == null || samples.size()==0) {
            return false;
        }
        return samples.stream().filter(s -> s.getTag()!=null).limit(2).count() == 2;
    }



}
