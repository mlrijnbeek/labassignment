package org.demo.lims.service.impl;

import org.demo.lims.dao.SampleTubeDao;
import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.SampleTube;
import org.demo.lims.service.SampleTubeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SampleTubeServiceImpl implements SampleTubeService {

    @Autowired
    private SampleTubeDao sampleTubeDao;

    @Override
    @Transactional
    public void create(SampleTube sampleTube) {
        validate(sampleTube);
        if (sampleTube.getSample()!=null)
            sampleTube.getSample().setSampleTube(sampleTube);
        sampleTubeDao.create(sampleTube);
    }

    @Override
    @Transactional
    public SampleTube findSampleTubeById(long id) {
        return sampleTubeDao.getSampleTubeById(id);
    }

    @Override
    @Transactional
    public List<SampleTube> findSampleTubeByBarCode(String barCode) {
        return sampleTubeDao.getSampleTubeByBarCode(barCode);
    }

    @Override
    public void validate(SampleTube sampleTube) {
        if (sampleTube.getBarCode()==null) {
            throw new ValidationException("Sample tube barcode must not be null");
        }
        if (!sampleTube.getBarCode().startsWith("NT")) {
            throw new ValidationException("Sample tube barcode must start with NT");
        }

    }

}
