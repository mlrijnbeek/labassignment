package org.demo.lims.service.impl;

import org.demo.lims.dao.PlateDao;
import org.demo.lims.dao.SampleTubeDao;
import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.*;
import org.demo.lims.service.PlateService;
import org.demo.lims.service.SampleTubeService;
import org.demo.lims.service.WellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlateServiceImpl implements PlateService {

    public final static int VALID_WELL_AMOUNT_96 = 96;
    public final static int VALID_WELL_AMOUNT_384 = 384;

    @Autowired
    private PlateDao plateDao;

    @Autowired
    private WellService wellService;

    @Override
    public void create(Plate plate) {
        validate(plate);
        plateDao.create(plate);

        if (plate.getWells() != null) {
            for (Well well : plate.getWells()) {
                well.setPlate(plate);
                wellService.create(well);
            }
        }
        plate = plateDao.getPlateById(plate.getId());
    }

    @Override
    @Transactional
    public     List<Plate> findPlateByBarCode(String barCode) {
        return  plateDao.findPlateByBarCode(barCode);
    }

    @Override
    public void validate(Plate plate) {
        if (plate.getBarCode()==null) {
            throw new ValidationException("Plate barcode must not be null");
        }
        if (!plate.getBarCode().startsWith("DN")) {
            throw new ValidationException("Plate barcode must start with DN");
        }

//        if (plate.getWells()!=null) {
//            if (plate.getWells().size()!= VALID_WELL_AMOUNT_96 &&
//                    plate.getWells().size()!= VALID_WELL_AMOUNT_384) {
//                throw new ValidationException("Invalid number of wells. Must be in : " +
//                        VALID_WELL_AMOUNT_96 + ", " + VALID_WELL_AMOUNT_384);
//            }
//        }

    }

    @Override
    public Plate findPlateById(long id) {
        return plateDao.getPlateById(id);
    }

}
