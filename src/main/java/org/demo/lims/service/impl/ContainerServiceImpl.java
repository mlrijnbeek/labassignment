package org.demo.lims.service.impl;

import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Container;
import org.demo.lims.model.Sample;
import org.demo.lims.service.ContainerService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class ContainerServiceImpl implements ContainerService{

    @Override
    public void tagCheck(Collection<Sample> samples) {
        if (samples!=null && samples.size()>1) {
            Set<String> tags = new HashSet<>();
            for (Sample s : samples) {
                if (s.getTag()!=null) {
                    if (tags.contains(s.getTag().getSequence())) {
                        throw new ValidationException("Cannot contain two samples with the same tag");
                    }
                    else {
                        tags.add(s.getTag().getSequence());
                    }
                }
            }
        }
    }

}
