package org.demo.lims.service;

import org.demo.lims.model.Sample;
import java.util.Collection;

public interface ContainerService {

    void tagCheck(Collection<Sample> samples);

}
