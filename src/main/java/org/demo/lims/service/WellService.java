package org.demo.lims.service;

import org.demo.lims.model.Well;

public interface WellService {

    void create(Well well);

    void validate(Well well);

}
