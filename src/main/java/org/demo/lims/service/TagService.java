package org.demo.lims.service;

import org.demo.lims.model.Tag;

public interface TagService {

    void create(Tag tag);

    void tagSample (Long tagId, Long sampleId);

    void validate(Tag tag);

    Tag getTagById(long id);


}
