package org.demo.lims.service;

import org.demo.lims.model.Sample;

import java.util.Set;

public interface SampleService {

    void create(Sample sample);

    void validate(Sample sample);

    Sample getSampleById(long id);

    void delete(long id);

    boolean isSampleSetWithMutipleTags(Set<Sample> samples);

}
