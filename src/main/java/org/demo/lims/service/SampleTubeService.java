package org.demo.lims.service;

import org.demo.lims.model.SampleTube;

import java.util.List;

public interface SampleTubeService {

    void create(SampleTube sampleTube);

    SampleTube findSampleTubeById(long id);

    List<SampleTube> findSampleTubeByBarCode(String barCode);

    void validate (SampleTube sampleTube);


}
