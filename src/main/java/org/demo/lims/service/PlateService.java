package org.demo.lims.service;

import org.demo.lims.model.Plate;
import java.util.List;

public interface PlateService {

    void create(Plate plate);

    void validate (Plate plate);

    Plate findPlateById(long id);

    List<Plate> findPlateByBarCode(String barCode);


}
