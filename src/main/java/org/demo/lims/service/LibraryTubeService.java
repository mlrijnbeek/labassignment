package org.demo.lims.service;

import org.demo.lims.model.LibraryTube;
import org.demo.lims.model.Sample;

import java.util.List;

public interface LibraryTubeService {

    void create(LibraryTube libraryTube);

    LibraryTube findLibraryTubeById(long id);

    List<LibraryTube> findLibraryTubeByBarCode(String barCode);

    void validate(LibraryTube libraryTube);

    void addSample(Long libraryTubeId, Sample sample);

    void removeSample(Long libraryTubeId, Long sampleId);

    void moveAllSamples(Long fromTubeId, Long toTubeId);

    void moveSample(Long fromTubeId, Long toTubeId, Long sampleId);

}
