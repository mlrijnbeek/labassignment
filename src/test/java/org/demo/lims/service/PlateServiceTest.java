package org.demo.lims.service;

import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Plate;
import org.demo.lims.model.Well;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlateServiceTest {

    @Autowired
    PlateService plateService;

    @Autowired
    WellService wellService;

    @Test (expected = ValidationException.class)
    public void create_invalidPlateBarcode_exception () {
        Plate plate = new Plate();
        plate.setBarCode("FAIL");
        plateService.create(plate);
    }

    @Test
    public void create_96Wells_success () {
        Plate plate = new Plate();
        plate.setBarCode("DN1234");
        plate.setWells(new HashSet<>());

        for (int i = 0; i < 96; i++) {
            Well well = new Well();
            well.setWellCoordinate("XY"+i);
            plate.getWells().add(well);
        }

        plateService.create(plate);
        Plate retrievedPlate = plateService.findPlateById(plate.getId());
        assertEquals(96,retrievedPlate.getWells().size());
    }

}
