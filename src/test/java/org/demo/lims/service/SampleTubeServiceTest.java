package org.demo.lims.service;

import org.demo.lims.model.Sample;
import org.demo.lims.model.SampleTube;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleTubeServiceTest {

    @Autowired
    SampleTubeService sampleTubeService;

    @Test
    public void create_validSampleTube_success () {
        SampleTube tube = new SampleTube();
        tube.setBarCode("NT123456789");
        Sample sample = new Sample();
        sample.setName("TEST");
        sample.setSampleTube(tube);
        tube.setSample(sample);
        sampleTubeService.create(tube);
        assertNotNull("expecting auto assigned id ", tube.getId());
        assertNotNull("expecting sample auto assigned id ", tube.getSample().getId());

    }

    @Test
    public void findByBarCode_barCodeExists_findOne () {
        SampleTube tube = new SampleTube();
        String barCode="NT111111111";
        tube.setBarCode(barCode);
        sampleTubeService.create(tube);
        assertNotNull("expecting auto assigned id ", tube.getId());

        List<SampleTube> list = sampleTubeService.findSampleTubeByBarCode(barCode);
        assertEquals(1,list.size());
    }


}
