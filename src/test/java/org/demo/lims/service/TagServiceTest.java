package org.demo.lims.service;

import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Plate;
import org.demo.lims.model.Sample;
import org.demo.lims.model.SampleTube;
import org.demo.lims.model.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TagServiceTest {

    @Autowired
    TagService tagService;

    @Autowired
    SampleTubeService sampleTubeService;


    @Autowired
    SampleService sampleService;

    @Test(expected = ValidationException.class)
    public void validate_invalidTagSequence_exception() {
        Tag t = new Tag();
        t.setSequence("BLADIBLA");
        tagService.validate(t);
    }

    @Test
    public void validate_validTagSequence_success() {
        Tag t = new Tag();
        t.setSequence("ACCGGTT");
        tagService.validate(t);
        assertNotNull(t);
    }

    @Test
    public void create_validTag_success() {
        Tag t = new Tag();
        t.setSequence("ACCGGTTAAAATCGTAAAA");
        tagService.create(t);
        assertNotNull("expecting auto assigned id ", t.getId());
    }

    @Test
    public void addTag_validTagAndSample_success() {
        SampleTube tube = new SampleTube();
        tube.setBarCode("NT123456789");
        Sample sample = new Sample();
        sample.setName("SampleTest12323");
        sample.setSampleTube(tube);
        tube.setSample(sample);
        sampleTubeService.create(tube);
        Tag tag = new Tag();
        tag.setSequence("ACCGGTTAAAATCGT");
        tagService.create(tag);
        tagService.tagSample(tag.getId(), sample.getId());

        Tag updatedTag = tagService.getTagById(tag.getId());
        assertEquals(1, updatedTag.getSamples().size());

    }
}
