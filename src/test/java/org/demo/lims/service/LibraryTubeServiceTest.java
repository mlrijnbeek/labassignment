package org.demo.lims.service;

import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Sample;
import org.demo.lims.model.LibraryTube;
import org.demo.lims.model.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LibraryTubeServiceTest {

    @Autowired
    LibraryTubeService libraryTubeService;

    @Autowired
    TagService tagService;

    @Test
    public void create_validLibraryTubeWithSamples_success () {
        LibraryTube tube = new LibraryTube();
        tube.setBarCode("NT123456789");
        tube.setSamples(new HashSet<>());
        for (int i = 0; i < 3; i++) {
            Sample sample = new Sample();
            sample.setName("TEST" + i);
            tube.getSamples().add(sample);
        }
        libraryTubeService.create(tube);
        assertNotNull("expecting auto assigned id ", tube.getId());
        assertNotNull(tube.getSamples());
        assertEquals(3, tube.getSamples().size());

        for (Sample s : tube.getSamples()) {
            assertNotNull("Expect sample id assigned ", s.getId());
        }
    }

    @Test
    public void findByBarCode_barCodeExists_findOne () {
        LibraryTube tube = new LibraryTube();
        String barCode="NT111111111";
        tube.setBarCode(barCode);
        libraryTubeService.create(tube);
        assertNotNull("expecting auto assigned id ", tube.getId());

        List<LibraryTube> list = libraryTubeService.findLibraryTubeByBarCode(barCode);
        assertEquals(1,list.size());
    }


    @Test (expected = ValidationException.class)
    public void moveSample_twoTaggedSamples_notAllowed () {
        // Create a library tube with 3 samples
        LibraryTube tube1 = new LibraryTube();
        tube1.setBarCode("NT999991");
        tube1.setSamples(new HashSet<>());
        for (int i = 1; i < 4; i++) {
            Sample sample = new Sample();
            sample.setName("TEST_A_" + i);
            tube1.getSamples().add(sample);
        }
        libraryTubeService.create(tube1);

        // Tag two of the samples
        Tag tag1 = new Tag();
        tag1.setSequence("AGGGGGGC");
        tagService.create(tag1);
        Object[] sampleArr = tube1.getSamples().toArray();

        tagService.tagSample(tag1.getId(), ((Sample)sampleArr[0]).getId());
        tagService.tagSample(tag1.getId(), ((Sample)sampleArr[2]).getId());
        tube1 = libraryTubeService.findLibraryTubeById(tube1.getId());

        // Create an empty target tube
        LibraryTube tube2 = new LibraryTube();
        tube2.setBarCode("NT999992");
        tube2.setSamples(new HashSet<>());
        libraryTubeService.create(tube2);

        // Rule: can't move a sample if two or more in source tube are tagged
        libraryTubeService.moveSample(tube1.getId(), tube2.getId(), ((Sample)sampleArr[0]).getId() );
    }

    @Test
    @Transactional
    public void moveSample_oneTaggedSamples_hurray () {
        // Create a library tube with 3 samples
        LibraryTube tube1 = new LibraryTube();
        tube1.setBarCode("NT000001");
        tube1.setSamples(new HashSet<>());
        for (int i = 1; i < 4; i++) {
            Sample sample = new Sample();
            sample.setName("TEST_XYZ_" + i);
            tube1.getSamples().add(sample);
        }
        libraryTubeService.create(tube1);

        // Tag two of the samples
        Tag tag1 = new Tag();
        tag1.setSequence("AGGTTTTC");
        tagService.create(tag1);
        Object[] sampleArr = tube1.getSamples().toArray();

        tagService.tagSample(tag1.getId(), ((Sample)sampleArr[2]).getId());
        tube1 = libraryTubeService.findLibraryTubeById(tube1.getId());

        // Create an empty target tube
        LibraryTube tube2 = new LibraryTube();
        tube2.setBarCode("NT000002");
        tube2.setSamples(new HashSet<>());
        libraryTubeService.create(tube2);

        libraryTubeService.moveSample(tube1.getId(), tube2.getId(), ((Sample)sampleArr[0]).getId() );

        tube1 = libraryTubeService.findLibraryTubeById(tube1.getId());
        tube2 = libraryTubeService.findLibraryTubeById(tube2.getId());

        assertEquals(2, tube1.getSamples().size());
        assertEquals(1, tube2.getSamples().size());
  }

}


