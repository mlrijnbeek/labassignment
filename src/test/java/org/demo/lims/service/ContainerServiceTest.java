package org.demo.lims.service;

import org.demo.lims.exception.ValidationException;
import org.demo.lims.model.Sample;
import org.demo.lims.model.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ContainerServiceTest {

    @Autowired
    ContainerService containerService;

    @Test
    public void tagCheck_emptySampleCollection_invalid() {
        List<Sample> sampleList = new ArrayList();
        containerService.tagCheck(sampleList);
        assertTrue("No validation error", true);

    }

    @Test (expected = ValidationException.class)
    public void tagCheck_doubleTag_invalid() {
        List<Sample> sampleList = new ArrayList();
        Tag tag = new Tag();
        tag.setSequence("AAGGCCTT");

        Sample s1 = new Sample();
        s1.setName("Sample1");
        s1.setTag(tag);

        Sample s2 = new Sample();
        s2.setName("Sample2");

        Sample s3 = new Sample();
        s3.setName("Sample3");
        s3.setTag(tag);

        sampleList.add(s1);
        sampleList.add(s2);
        sampleList.add(s3);

        containerService.tagCheck(sampleList);
    }
}
