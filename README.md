Modelling Labware and Containers
--------------------------------
This is an implementation of a specification to implement labware and containers.
Technology stack:
  - Java 8
  - Spring Boot REST service
  - Hibernate
  - H2 in-memory database

Building the service
--------------------
This is a Maven build:
* command line: use "mvn clean install" to build and test the project.
* IDE: open the project (pom file) and build the project there. Default is for Intellij, project file included.

Running the service
-------------------
Open the project in your IDE and run class "WebApplication"
This is a Spring Boot application that will start a service on default port 8080.


API documentation (Swagger)
---------------------------
After starting the service
* See: http://localhost:8080/v2/api-docs
* http://localhost:8080/swagger-ui.html#

Invoking the service
--------------------
A Postman collection is included in file "Lims.postman_collection"
A number of requests are included. A scenario of requests would be:
* Create Library Tube (creates a library tunbe with samples)
* Create Tag (create a tag)
* Add tag to sample (associates tag with a sample)
* Find Library tube by id (shows the tagged sample)


Notes on the implementation
----------------------------
* The project has a default Maven strucure with 'main' and 'test'
  It follows the standard MVC structure (service, model, dao, controller packages)
* The "model" package contains the various entities, modeled (interpreted) from the specification.
* The model could be split - one core model (with the JPA annotations) and one to serve the REST
  service (JSON annotated). This would allow for a better split in purpose.
* When started, a new H2 database is created on the fly. This can be changed through application.properties, spring.jpa.hibernate.ddl-auto = create
* All data is persisted to the database, using Hibernate and H2. ID management is done through the database.
* A number of unit tests are in place, these mainly check the correct working of various 'service' classes
* Validation of business rules tends to happen in the various "Service" classes


Tasks
-----
Required functionality:
- Model all of the entities listed above.
  _See: package org.demo.lims.model_
  
- Be able to create plates (and their wells), sample tubes and library tubes.
  _See package org.demo.lims.controller (POST methods) and Postman examples ("Create ...")_
  
- Add and remove sample(s) from a container.
  _Implementation example in LibraryTubeController.deleteSampleFromLibraryTube_  

- Add tags to samples.
  _Implementation example in TagController.addTagToSample, see also Postman._
  
- Search for a container by its barcode.
  _Implementation example in SampleTubeController.findSampleTubeByBarcode._

- Obtain a list of samples (and tags) for a plate or tube.
  _Implementation example in LibaryTubeController.findSampleTubeById, see also Postman._

- Move sample(s) from one container to another.
  _Implementation example in LibaryTubeController.moveAllSamples and moveSample.
  Check Unit tests as well (LibraryTubeServiceTest)_
